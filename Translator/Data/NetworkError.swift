//
//  NetworkError.swift
//  Translator
//
//  Created by Estiven on 16/08/23.
//

import Foundation

enum NetworkError: Error {
    case invalidURL
        case serverError(Error)
        case noData
        case decodingError(Error)
        case encodingError(Error)
}
