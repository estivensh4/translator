//
//  TranslatorApp.swift
//  Translator
//
//  Created by Estiven on 16/08/23.
//

import SwiftUI

@main
struct TranslatorApp: App {

    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
