//
//  LanguageDetectInDTO.swift
//  Translator
//
//  Created by Estiven on 16/08/23.
//

import Foundation


struct LanguageDetectInDTO: Codable {
    
    var data : LanguageDetectData = LanguageDetectData()
    
    enum CodingKeys: String, CodingKey {
        
        case data = "data"
        
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        
        data = try values.decodeIfPresent(LanguageDetectData.self , forKey: .data ) ?? LanguageDetectData()
        
    }
    
    init() {
        
    }
    
}

struct Detections: Codable {
    

    var language   : String = ""
    
    enum CodingKeys: String, CodingKey {
        
        case language   = "language"
        
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        language   = try values.decodeIfPresent(String.self , forKey: .language   ) ?? ""
        
    }
    
    init() {
        
    }
    
}

struct LanguageDetectData: Codable {
    
    var detections : [[Detections]] = []
    
    enum CodingKeys: String, CodingKey {
        
        case detections = "detections"
        
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        
        detections = try values.decodeIfPresent([[Detections]].self , forKey: .detections ) ?? []
        
    }
    
    init() {
        
    }
    
}
