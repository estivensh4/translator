//
//  TranslateTextOutDTO.swift
//  Translator
//
//  Created by Estiven on 16/08/23.
//

import Foundation

struct TranslateTextInDTO: Codable {
    var data : TranslateTextData = TranslateTextData()
    
    enum CodingKeys: String, CodingKey {
        
        case data = "data"
        
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        
        data = try values.decodeIfPresent(TranslateTextData.self , forKey: .data ) ?? TranslateTextData()
        
    }
    
    init() {
        
    }
}

struct TranslateTextData: Codable {
    
    var translations : [Translations] = []
    
    enum CodingKeys: String, CodingKey {
        
        case translations = "translations"
        
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        
        translations = try values.decodeIfPresent([Translations].self , forKey: .translations ) ?? []
        
    }
    
    init() {
        
    }
    
}

struct Translations: Codable {
    
    
    var translatedText   : String = ""
    
    enum CodingKeys: String, CodingKey {
        case translatedText   = "translatedText"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        translatedText   = try values.decodeIfPresent(String.self , forKey: .translatedText   ) ?? ""
        
    }
    
    init() {
        
    }
    
}
