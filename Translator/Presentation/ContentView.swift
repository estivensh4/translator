//
//  ContentView.swift
//  Translator
//
//  Created by Estiven on 16/08/23.
//

import SwiftUI
import CoreData

struct ContentView: View {
    
    @State private var source: String = ""
    @State private var target: String = ""
    @StateObject private var translatorViewModel = TranslatorViewModel()
    
    var body: some View {
        NavigationStack {
            VStack {
                TextField(
                    "Escribe o pega el texto aquí para traducirlo",
                    text: $source
                ).font(.title2)
                    .padding(.horizontal, 16)
                    .onChange(of: source) {newValue in
                        translatorViewModel.onEvent(event: .detectLenguage(text: newValue))
                        if !translatorViewModel.languageDetect.isEmpty {
                            translatorViewModel.onEvent(event: .translateText(text: newValue, source: translatorViewModel.languageDetect))
                            target = translatorViewModel.textResult
                        }
                        
                    }
                if !source.isEmpty {
                    Divider()
                    TextField(
                        "",
                        text: $target
                    ).font(.title2).padding(.horizontal, 16)
                }
            }
            
            .toolbar {
                ToolbarItem(placement: .principal) {
                    HStack(spacing: 30) {
                        VStack {
                            Text("Detectar idioma")
                                .foregroundColor(.white)
                            if !translatorViewModel.languageDetect.isEmpty {
                                Text(translatorViewModel.languageDetect)
                                    .font(.caption)
                                    .foregroundColor(.white)
                            }
                        }
                        
                        Image(systemName: "arrow.right") .foregroundColor(.white)
                        
                        Text("Ingles")
                            .foregroundColor(.white)
                    }
                }
            }
            .navigationBarTitleDisplayMode(.inline)
            .toolbarBackground(Color("ColorPrimary"), for: .navigationBar)
            .toolbarBackground(.visible, for: .navigationBar)
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
