//
//  TranslatorViewModel.swift
//  Translator
//
//  Created by Estiven on 16/08/23.
//

import Foundation
class TranslatorViewModel: ObservableObject {
    
    @Published var languageDetect = ""
    @Published var textResult = ""
    
    
    func onEvent(event: TranslatorEvents){
        switch event {
        case .detectLenguage(let text):
            detectLanguage(text: text)
        case .translateText(let text, let source):
            translateText(text: text, source: source)
        }
    }
    
    private func detectLanguage(text: String) {
        
        NetworkManager.shared.postData(endpoint: "v2/detect?key=AIzaSyDJyAnmtcbV5Ek1QRnw485BMC1bVb8PaFc", data: ["q": text]) { (result: Result<LanguageDetectInDTO, NetworkError>) in
            
            switch result {
            case .success(let result):
                DispatchQueue.main.async {
                    self.languageDetect = result.data.detections.first?.first?.language ?? ""
                }
                
            case .failure(let error):
                print("error \(error)")
            }
        }
    }
    
    private func translateText(
        text: String,
        source: String
    ) {
        
        let translateTextOutDTO = [
            "key": "AIzaSyDJyAnmtcbV5Ek1QRnw485BMC1bVb8PaFc",
            "q": text,
            "source": source,
            "target": "en",
            "format": "text"
        ]
        
        NetworkManager.shared.postData(endpoint: "v2?key=AIzaSyDJyAnmtcbV5Ek1QRnw485BMC1bVb8PaFc", data: translateTextOutDTO) { (result: Result<TranslateTextInDTO, NetworkError>) in
            
            switch result {
            case .success(let result):
                DispatchQueue.main.async {
                    self.textResult = result.data.translations.first?.translatedText ?? ""
                }
                
            case .failure(let error):
                print("error \(error)")
            }
        }
    }
}
