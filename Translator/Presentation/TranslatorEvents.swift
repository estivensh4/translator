//
//  TranslatorEvents.swift
//  Translator
//
//  Created by Estiven on 16/08/23.
//

import Foundation

enum TranslatorEvents {
    case detectLenguage(text: String)
    case translateText(text: String,source: String)
}

